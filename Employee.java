package com.girnarsoft.employees;

public class Employee {
	private int empId;
	private String name;
	private long mobileNumber;
	protected double salary;
	private String role;
	
	public Employee(int empId, String name, long mobileNumber, double salary, String role) {
		this.empId = empId;
		this.name = name;
		this.mobileNumber = mobileNumber;
		this.salary = salary;
		this.role = role;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public long getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	

}	