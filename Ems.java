package com.girnarsoft.employees;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Ems {

	public static void main(String[] args) {
		Map<Integer, Employee> hm = getEmployeeMap();
		Login obj = new Login();
		obj.login();
	}
	public static Map<Integer, Employee> getEmployeeMap(){
		Map<Integer,Employee> hm = new HashMap<>(); 
		Employee e1 = new Employee(1001,"Ria",99295089,5010.10,"ceo");
		hm.put(e1.getEmpId(), e1);
		Employee e2 = new Employee(1002,"Pia",99295090,4000.00,"director");
		hm.put(e2.getEmpId(), e2);
		Employee e3 = new Employee(1003,"Sia",99295091,3050.50,"hr");
		hm.put(e3.getEmpId(), e3);
		Employee e4 = new Employee(1004,"Tia",99295092,2456.78,"manager");
		hm.put(e4.getEmpId(), e4);
		Employee e5 = new Employee(1005,"Dia",99295093,3156.78,"employee");
		hm.put(e5.getEmpId(), e5);
		
		return hm;
	}
	
	public static void EmployeeList() {
	 Map<Integer, Employee> hm = null;
	for(Map.Entry<Integer, Employee> entry:hm.entrySet()){    
	        int key=entry.getKey();  
	        Employee e=entry.getValue();  
	        System.out.println(key+" Details:");  
	        System.out.println("Name: " + e.getName());
	        System.out.println("Mobile Number: " + e.getMobileNumber());
	        System.out.println("Salary: " + e.getSalary());
	    }     
	  }
	 
}
