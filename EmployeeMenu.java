package com.girnarsoft.employees;

import java.util.Scanner;

public class EmployeeMenu {
	protected static void employee_menu()
	{
		Scanner in = new Scanner(System.in);
		int employee_menu_choice;

		System.out.println("What role do you identify with?");
		System.out.println("Choose from the options below:");
		System.out.print("1. CEO \n2. Director \n3. HR \n4. Manager \n5. Employee \n6. Logout");
		employee_menu_choice = in.nextInt();
		
		switch(employee_menu_choice)
		{
		case 1:
		{
			Ceo ceo1 = new Ceo();
			ceo1.tasks_ceo();
			break;
		}
		case 2:
		{
		//	tasks_director();
			break;
		}
		case 3:
		{
		//	tasks_hr();
			break;
		}
		case 4:
		{
		//	tasks_manager();
			break;
		}
		case 5:
		{
		//	tasks_employee();
			break;
		}
		case 6:
		{
			System.out.println("You've been successfully logged out");
			return;
		}
		default:
			System.out.println("Please enter a valid option");
			employee_menu();
		}
	}

}
